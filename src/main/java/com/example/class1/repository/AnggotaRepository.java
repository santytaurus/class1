package com.example.class1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.class1.model.AnggotaModel;

public interface AnggotaRepository extends JpaRepository<AnggotaModel, String> {
	
	@Query("SELECT F FROM AnggotaModel F WHERE F.kodeAnggota = ?1") // ?1 untuk mencari semua data
	AnggotaModel searchKodeAnggota(String kodeAnggota);
	
	@Query("SELECT F FROM AnggotaModel F WHERE F.kodeAnggota =?1")
	AnggotaModel searchkodeAnggota(String kodeAnggota);
	
	@Query("SELECT F FROM AnggotaModel F WHERE F.namaAnggota =?1")
	AnggotaModel searchnamaAnggota(String namaAnggota);
	
	@Query("SELECT F FROM AnggotaModel F WHERE F.usiaAnggota =?1")
	AnggotaModel searchusiaAnggota(Integer usiaAnggota);
	
	@Query("SELECT F FROM AnggotaModel F WHERE F.namaAnggota LIKE %?1% ") //'%?1%' untuk mencari suatu data yang berawal dari ?1 / bisa jadi mencari data yang berawal huruf 'ad'
	List<AnggotaModel> searchNamaAnggota(String namaAnggota);

	@Query("SELECT F FROM AnggotaModel F WHERE F.kodeAnggota LIKE %?1%")
	List<AnggotaModel> cariKodeAnggotaRepository(String kodeAnggota);
}
