package com.example.class1.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.class1.model.AnggotaModel;
import com.example.class1.model.FakultasModel;
import com.example.class1.service.AnggotaService;
import com.example.class1.service.FakultasService;

import java.util.List;
import java.util.ArrayList;

@Controller
@RequestMapping ("/anggota")
public class AnggotaController {
	
	
	@Autowired
	private AnggotaService anggotaService;
	
	@Autowired
	private FakultasService fakultasService;
	
	@RequestMapping("/home")
	public String anggota()
	{
		return "/anggota/home";
	}
	
	@RequestMapping("/add")
	public String doAdd(Model model) {
		this.doListFakultas(model);
		return "/anggota/add";
	}
	
	
	@RequestMapping("/create")
	public String doCreate(HttpServletRequest request) {
		int idAnggota = Integer.parseInt(request.getParameter("idAnggota"));
		String kodeAnggota = request.getParameter("kodeAnggota");
		String namaAnggota = request.getParameter("namaAnggota");
		int usiaAnggota = Integer.parseInt(request.getParameter("usiaAnggota"));
		Integer kodeFakultas = Integer.parseInt(request.getParameter("kodeFakultas"));
		
		AnggotaModel anggotaModel = new AnggotaModel();
		anggotaModel.setIdAnggota(idAnggota);
		anggotaModel.setKodeAnggota(kodeAnggota);
		anggotaModel.setNamaAnggota(namaAnggota);
		anggotaModel.setUsiaAnggota(usiaAnggota);
		anggotaModel.setKodeFakultas(kodeFakultas);	
		
		this.anggotaService.save(anggotaModel);
		
		return "/anggota/home";
	}
	
	@RequestMapping("/data")
	public String doList(Model model)
	{
		List<AnggotaModel> anggotaModelList = new ArrayList<AnggotaModel>();
		anggotaModelList = this.anggotaService.read();
		model.addAttribute("anggotaModelList", anggotaModelList);
		
		return "/anggota/list";
	}
	
	public void doListFakultas(Model model) {
		List<FakultasModel> fakultasModelList = new ArrayList<FakultasModel>();
		fakultasModelList = this.fakultasService.read();
		model.addAttribute("fakultasModelList", fakultasModelList);		
	}
	
	
	
	@RequestMapping("/detail")
	public String doDetail(HttpServletRequest request, Model model)
	{
		String kodeAnggota = request.getParameter("kodeAnggota");
		AnggotaModel anggotaModel = new AnggotaModel();
		anggotaModel = this.anggotaService.searchKodeAnggota(kodeAnggota);
		model.addAttribute("anggotaModel", anggotaModel);
		
		return "/anggota/detail";
	}
	
	@RequestMapping("/ubah")
	public String doUbah(HttpServletRequest request, Model model)
	{
		String kodeAnggota = request.getParameter("kodeAnggota");
		AnggotaModel anggotaModel = new AnggotaModel();
		anggotaModel = this.anggotaService.searchKodeAnggota(kodeAnggota);
		model.addAttribute("anggotaModel", anggotaModel);
		
		return "/anggota/edit";
	}
	
	@RequestMapping("/update")
	public String doUpdate(HttpServletRequest request, Model model)
	{
	String kodeAnggota = request.getParameter("kodeAnggotas");
	String namaAnggota = request.getParameter("namaAnggota");
	String usiaAnggota = request.getParameter("usiaAnggota");
	
	AnggotaModel anggotaModel = new AnggotaModel();
	anggotaModel.setKodeAnggota(kodeAnggota);
	anggotaModel.setNamaAnggota(namaAnggota);
	anggotaModel.setNamaAnggota(usiaAnggota);
	this.anggotaService.update(anggotaModel);
	
	return "/anggota/home";
	
	}
	
	@RequestMapping("/hapus")
	public String doHapus(HttpServletRequest request, Model model){
	String kodeAnggota = request.getParameter("kodeAnggota");
	AnggotaModel anggotaModel = new AnggotaModel();
	anggotaModel = this.anggotaService.searchKodeAnggota(kodeAnggota);
	this.anggotaService.delete(anggotaModel);
	
	return "/anggota/home";
	
	}
	
	
	@RequestMapping("/search")
	public String doSearchNama(HttpServletRequest request, Model model){
	String namaAnggota= request.getParameter("namaAnggota");
	
	List<AnggotaModel> anggotaModelList = new ArrayList<AnggotaModel>();
	anggotaModelList = this.anggotaService.searchNamaAnggota(namaAnggota);
	model.addAttribute("anggotaModelList", anggotaModelList);
	
	return "/anggota/search";
	
	}
	@RequestMapping("/cari/kode")
	public String cariKode(HttpServletRequest request, Model model) {
		String kodeController = request.getParameter("kodeHTML");
		System.out.println(kodeController);
		List<AnggotaModel> anggotaModelList = this.anggotaService.cariKodeAnggotaDonk(kodeController);
		model.addAttribute("anggotasModelList",anggotaModelList);
		return "/anggota/home";
	}
	
}
