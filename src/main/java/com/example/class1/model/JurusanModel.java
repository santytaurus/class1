package com.example.class1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="T_Jurusan")
public class JurusanModel {

	@Id
	@Column(name="kd_Jurusan",nullable = false, length = 10)
	private String kodeJurusan;
	
	@Column(name="nm_Jurusan")
	private String namaJurusan;
	
	@Column(name="kd_Fakultas",nullable = false, length = 25)
	private String kodeFakultas;
	
	@Column(name="grd_Jurusan")
	private int gradeJurusan;
	
	public String getKodeJurusan() {
		return kodeJurusan;
	}
	public void setKodeJurusan(String kodeJurusan) {
		this.kodeJurusan = kodeJurusan;
	}
	public String getNamaJurusan() {
		return namaJurusan;
	}
	public void setNamaJurusan(String namaJurusan) {
		this.namaJurusan = namaJurusan;
	}
	public String getKodeFakultas() {
		return kodeFakultas;
	}
	public void setKodeFakultas(String kodeFakultas) {
		this.kodeFakultas = kodeFakultas;
	}
	public int getGradeJurusan() {
		return gradeJurusan;
	}
	public void setGradeJurusan(int gradeJurusan) {
		this.gradeJurusan = gradeJurusan;
	}
	
}
