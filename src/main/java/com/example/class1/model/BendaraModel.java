package com.example.class1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="T_Bendara")

public class BendaraModel {
	
	@Id
	@Column(name="KD_Bendara")
	private String kodeBendara;

	@Column(name="NM_Bendara",nullable = false, length = 25)
	private String namaBendara;
	
	public String getKodeBendara() {
		return kodeBendara;
	}
	public void setKodeBendara(String kodeBendara) {
		this.kodeBendara = kodeBendara;
	}
	public String getNamaBendara() {
		return namaBendara;
	}
	public void setNamaBendara(String namaBendara) {
		this.namaBendara = namaBendara;
	}
	
	
}
