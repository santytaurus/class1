package com.example.class1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "T_Anggota")

public class AnggotaModel {
	
	@Id
	@Column(name="ID_Anggota", nullable =false)
	private int idAnggota;
	
	@Column(name="KD_Anggota")
	private String kodeAnggota;
	
	@Column(name="NM_Anggota")
	private String namaAnggota;
	
	@Column(name="USIA_Anggota")
	private int usiaAnggota;
	
	@Column(name = "kd_fakultas")
	private Integer kodeFakultas;
	
	@ManyToOne	
	@JoinColumn(name = "kd_fakultas", nullable=true, updatable=false, insertable=false)
	private FakultasModel fakultasModel;
	
	public int getIdAnggota() {
		return idAnggota;
	}

	public void setIdAnggota(int idAnggota) {
		this.idAnggota = idAnggota;
	}
	
	public String getKodeAnggota() {
		return kodeAnggota;
	}

	public void setKodeAnggota(String kodeAnggota) {
		this.kodeAnggota = kodeAnggota;
	}

	public String getNamaAnggota() {
		return namaAnggota;
	}

	public void setNamaAnggota(String namaAnggota) {
		this.namaAnggota = namaAnggota;
	}
	
	public int getUsiaAnggota() {
		return usiaAnggota;
	}

	public void setUsiaAnggota(int usiaAnggota) {
		this.usiaAnggota = usiaAnggota;
	}

	public Integer getKodeFakultas() {
		return kodeFakultas;
	}

	public void setKodeFakultas(Integer kodeFakultas) {
		this.kodeFakultas = kodeFakultas;
	}

	public FakultasModel getFakultasModel() {
		return fakultasModel;
	}

	public void setFakultasModel(FakultasModel fakultasModel) {
		this.fakultasModel = fakultasModel;
	}	
	
}
